/*
 * TempSensMega48.c
 *
 * Created: 2017. 10. 07. 17:00:50
 * Author : DANI
 */ 

#define F_CPU 8000000
#include <util/delay.h>

#include <avr/io.h>

#define FOSC 8000000			//Clock
#define BAUD 38400				//Boud rate
#define MYUBBR FOSC/16/BAUD-1	//Value of UBRR0H & UBRR0L

#define TXREADYMASK 0b00100000	//Mask for tx ready bit in UCSR0A
#define ADCREADYMASK 0b01000000	//Mask for ADC ADSC bit to check that conversation is  complete

#define Vref 3.3

void sendData(char* data) {
	int i = 0;
	while((data[i] != '\0') && (i < 100)) {
		if(UCSR0A & TXREADYMASK) {
			UDR0 = data[i];
			++i;
		}
	}
}

//Clears the fosc prescaler for 8 MHz clock
void setClock() {
	CLKPR = 0b10000000;	//Set CLKPE 1 to enable clock prescaler change
	CLKPR = 0;			//Set prescaler factor to 0. Clock frequency 8 MHz
}

//Sets up UART transmitter 8bit data, even parity, 1 stop bit
void initUART(unsigned int ubbr) {
	UBRR0H = (unsigned char)(ubbr >> 8);	//Sets baud rate register high byte
	UBRR0L = (unsigned char)ubbr;			//Sets baud rate register low byte
	UCSR0B |= 0b00001000;					//Enable UART transmit
	UCSR0C |= 0b00100110;					//UART 8bit operation with even parity and one stop bit
}

//Sets up ADC. Internal 1.1V reference ADC0 input channel
void initADC() {
	ADMUX = 0b01000111;		//Internal 1.1V reference, ADC0 as input channel
	ADCSRA = 0b00000110;	//ADC clock prescaler 64 --> 125 kHz ADC clock
	DIDR0 = 0b00111111;		//Turns off the digital input buffers of ADC0..5 to reduce power consumption	
}


unsigned int ADCSingleConversion() {
	unsigned int x;					//Variable to store ADC value
	PRR &= 0b11111110;				//Enable ADC power
	ADCSRA |= 0b11000000;			//Turn ADC on and start conservation
	while(ADCSRA & ADCREADYMASK);	//Wait until conservation complete
	x = ADCL;						//Low byte of ADC
	x |= (ADCH << 8);				//High byte of ADC
	char myADC[2];
	myADC[1] = '\0';
	sendData("ADCH: ");
	myADC[0] = (unsigned char) ((x >> 8) + 48);
	sendData(myADC);
	sendData("\r\nADCL: ");
	myADC[0] = (unsigned char) (x + 48);
	sendData(myADC);
	sendData("\r\n");
	ADCSRA &= 0b00000111;			//Turn ADC off
	PRR |= 0b00000001;				//Disable ADC power
	return x;						//Return a the result of the conservation
}

//Converts a double number between 0..1,1 to a BCD number on 2 byte
unsigned int doubleToBCD(double number) {
	unsigned int BCD = 0;			//The result will be stored in this variable
	unsigned int temp;				//For double to int conservation
	unsigned int i = 10;					//For the cycle
	unsigned char shift = 0;
	number *= 1000;					//Converting to mV for easier use
	temp = (unsigned int) number;
	
	while(temp > 0) {
		BCD |= (unsigned int)(temp % i) << shift;
		temp = (temp - (temp % i))/10;
		shift += 4;
	}
	
	return BCD;
}

void BCDToASCII(unsigned int number, char* result) {
	unsigned int i;
	unsigned int shift = 12;
	for(i = 0; i < 4; ++i) {
		result[i] = (unsigned char) (((number >> shift) & 0b00001111) + 48);
		shift -= 4;
	}
	result[4] = '\0';
}

unsigned int calculateAnalogVoltage(unsigned int x) {
	double Vin;
	Vin = (x * Vref)/1024.0;	//Calculate the measured voltage
	return doubleToBCD(Vin);	//Returns the measured voltage as a 4 digit BCD number in 2 byte
}

int main(void)
{
	unsigned int ADCValue;
	char voltage[5];
	setClock();
	initUART(MYUBBR);
	sendData("Uart initalization complete!\r\n");
	initADC();
	sendData("ADC initalization complete!\r\n");
    while (1) 
    {
		_delay_ms(2000);
		ADCValue = calculateAnalogVoltage(ADCSingleConversion());
		sendData("Measured voltage: ");
		BCDToASCII(ADCValue, voltage);
		sendData(voltage);
		sendData(" V\r\n");
    }
}

